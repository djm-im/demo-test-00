package im.djm.demotest.service;

import im.djm.demotest.repository.GreetingRepository;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class GreetingService {

    private final GreetingRepository greetingRepository;

    public GreetingService(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    public Map<String, String> greeting(String name) {

        return greetingRepository.greeting(name);
    }
}
